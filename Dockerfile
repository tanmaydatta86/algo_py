FROM python:alpine3.7
RUN apk add --no-cache curl python3 pkgconfig python3-dev openssl-dev libffi-dev musl-dev make gcc
RUN pip install --upgrade pip
RUN pip install cython numpy
COPY . /algo_tester
WORKDIR /algo_tester
RUN pip install -r requirements.txt
CMD python src/backtest.py